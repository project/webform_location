/**
 * @file
 * JavaScript behaviors for Webform Location HTML5 integration.
 */

(function ($, Drupal) {
  "use strict";

  /**
   * Initialize Location HTML5.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformLocationHTML5 = {
    attach: function (context) {
      $(context)
        .find(".js-webform-type-webform-location-html5")
        .once("webform-location-html5")
        .each(function () {
          var $element = $(this);
          var $input = $element.find(".webform-location-location_html5");

          function locationSucsses(position) {
            $input.val(
              position.coords.latitude + "," + position.coords.longitude
            );
          }

          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(locationSucsses);
          }
        });
    },
  };
})(jQuery, Drupal);
