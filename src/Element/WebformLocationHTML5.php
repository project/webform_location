<?php

namespace Drupal\webform_location_html5\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformLocationBase;

/**
 * Provides a webform element for a location HTML5 element.
 *
 * @FormElement("webform_location_html5")
 */
class WebformLocationHTML5 extends WebformLocationBase
{

  /**
   * {@inheritdoc}
   */
  protected static $name = 'location_html5';

  /**
   * {@inheritdoc}
   */
  public static function getCompositeElements(array $element)
  {
    $elements = [];

    $elements['value'] = [
      '#type' => 'textfield',
      '#title' => \Drupal::translation()->translate('Location'),
      '#attributes' => [
        'class' => ['webform-location-' . static::$name],
      ],
    ];

    $attributes = static::getLocationAttributes();
    foreach ($attributes as $name => $title) {
      $elements[$name] = [
        '#title' => $title,
        '#type' => 'textfield',
        '#error_no_message' => true,
        '#attributes' => [
          'data-webform-location-' . static::$name . '-attribute' => $name,
        ],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function processWebformComposite(&$element, FormStateInterface $form_state, &$complete_form)
  {
    $element = parent::processWebformComposite($element, $form_state, $complete_form);

    // Attach library.
    $element['#attached']['library'][] = 'webform_location_html5/webform_location_html5.element';

    return $element;
  }

}
