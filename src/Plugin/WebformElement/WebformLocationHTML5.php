<?php

namespace Drupal\webform_location_html5\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformLocationBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides an 'location' element using Geocomplete.
 *
 * @WebformElement(
 *   id = "webform_location_html5",
 *   label = @Translation("Location HTML5"),
 *   description = @Translation("Provides a form element for collecting valid location information (longitude, latitude) using the browser's HTML5 API."),
 *   category = @Translation("Composite elements"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 */
class WebformLocationHTML5 extends WebformLocationBase
{

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties()
  {
    return [
      'hidden' => false,
    ] + parent::defineDefaultProperties()
     + $this->defineDefaultBaseProperties();
  }

  /****************************************************************************/

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItem(array $element, WebformSubmissionInterface $webform_submission, array $options = [])
  {
    $value = $this->getValue($element, $webform_submission, $options);

    // Return empty value.
    if (empty($value) || empty(array_filter($value))) {
      return '';
    }

    return parent::formatHtmlItem($element, $webform_submission, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state)
  {
    $form = parent::form($form, $form_state);

    $form['composite']['geolocation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Use the browser's Geolocation as the default value"),
      '#description' => $this->t('The <a href="https://developer.mozilla.org/en-US/docs/Web/API/Geolocation_API">HTML Geolocation API</a> is used to get the geographical position of a user. Since this can compromise privacy, the position is not available unless the user approves it.'),
      '#return_value' => true,
    ];

    unset($form['composite']['hidden']);

    // Reverted #required label.
    $form['validation']['required']['#description'] = $this->t('Check this option if the user must enter a value.');

    return $form;
  }

}
